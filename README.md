## Overview ##

Heartbeat addon sends messages to Morpheus, to keep running VM instance alive, as long as there are clients connected to codebox workspace.
When last client disconnects, Morpheus will shutdown VM associated to this codebox workspace, after timeout.

## Install ##

To install addon, you must have codebox setup with core addons installed

    git clone git@bitbucket.org:Code911/codebox.git && cd codebox
    npm install
    npm install -g gulp jsctags
    export NODE_PATH=$NODE_PATH/jsctags/jsctags:$NODE_PATH
    gulp
    mkdir -p ~/.codebox/packages
    node ./bin/codebox.js install

In the command below, replace `<bitbucket user>` with actual bitbucket login.

        node ./bin/codebox.js install -p 'heartbeat:https://<bitbucket user>@bitbucket.org/Code911/codebox-heartbeat.git'

Finally, run codebox. Specifiy folder for workspace root.

        node ./bin/codebox.js run /some/folder

## Addon setup ##

Set following environment variables before launching codebox, to configure addon:

* `MORPHEUS_HOST=example.com` - Host to which the ping will be sent
* `INSTANCE_ID=abcdef` - EC2 instance id that will be sent to Morpheus
* `HEARTBEAT_INTERVAL=600000` - interval in ms at which heartbeat will be sent. Defaults to 10m (600000ms)


Export these variables, before launching codebox. Once the first client is connected, codebox will start sending heartbeat messages.'

    export MORPHEUS_HOST=example.com
    export INSTANCE_ID=abcdef
    export HEARTBEAT_INTERVAL=600000
    node ./bin/codebox.js run /some/folder



