'use strict';
var Socket = codebox.require("core/socket");

// Simply keep this connection open to let server know that client is alive
var hearbeat = new Socket({
    service: "heartbeat"
});
