'use strict';

var http = require('http');
var intervalId = null;
var clients = 0;

module.exports = function(codebox) {
    var host = process.env.MORPHEUS_HOST;
    var interval = parseInt(process.env.HEARTBEAT_INTERVAL || 600000);
    var instanceId = process.env.INSTANCE_ID;
    var authorization = process.env.AUTHORIZATION;
    var ping, port;

    if (host && instanceId) {
        host = host.split(':');
        codebox.logger.log(host);
        switch(host.length) {
        case 1:
            host = host[0];
            port = 80;
            break;
        case 2:
            port = parseInt(host[1]);
            host = host[0];
            codebox.logger.log(host, port);
            break;
        default:
            codebox.logger.error("Bad host:", process.env.MORPHEUS_HOST);
        }
        codebox.logger.log("pinging with", {
            hostname: host,
            port: port,
            method: "POST",
            path: '/instances/' + instanceId + '/ping'
        });

        ping = function() {
            var req = http.request({
                hostname: host,
                port: port,
                auth: authorization,
                method: "POST",
                path: '/instances/' + instanceId + '/ping'
            }, function(res) { codebox.logger.log(res);}); 
            req.end();
        }
    } else {
        codebox.logger.error("Connection to morpheus is not defined. Please set MORPHEUS_HOST and INSTANCE_ID environment variables");
    }
    codebox.socket.service("heartbeat", function(socket) {
        ++clients;
        if (typeof(ping) === 'function' && clients == 1) {
            intervalId = setInterval(ping, interval);
        }
        socket.on('close', function() {
            if (--clients <= 0 && intervalId) {
                clearInterval(intervalId);
                intervalId = null;
                clients = 0;
            }
        });
    });
};
